﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace OcelotDownAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OcelotController : ControllerBase
    {
        // GET api/ocelot/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await Task.Run(() =>
            {
                return $"This is from {HttpContext.Request.Host.Value}, path: {HttpContext.Request.Path}";
            });
            return Ok(result);
        }
        // GET api/ocelot/aggrWilling
        [HttpGet("aggrWilling")]
        public async Task<IActionResult> AggrWilling(int id)
        {
            var result = await Task.Run(() =>
            {
                return $"我是Willing，还是多加工资最实际, path: {HttpContext.Request.Path}";
            });
            return Ok(result);
        }
        // GET api/ocelot/aggrJack
        [HttpGet("aggrJack")]
        public async Task<IActionResult> AggrJack(int id)
        {
            var result = await Task.Run(() =>
            {
                return $"我是Jack，我非常珍惜现在的工作机会, path: {HttpContext.Request.Path}";
            });
            return Ok(result);
        }
    }
}
